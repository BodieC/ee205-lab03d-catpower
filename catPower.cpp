////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  01 Jan 2022 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

// A Joule represents the amount of electricity required to run a 1 W device for 1 s

const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;
const double MEGATON_TNT_IN_A_JOULE = 1/4.184e15;
const double GAS_GAL_IN_A_JOULE= 1/1.213e8;
const double SUPERNOVA_IN_A_JOULE = 1/1e24;
const double CATPOWER_IN_A_JOULE= 0;

const char JOULE         = 'j';
const char ELECTRON_VOLT = 'e';
const char MEGATON_TNT = 'm';
const char GAS_GAL_EQ = 'g';
const char SUPERNOVA = 'f';
const char CATPOWER = 'c';

//working
double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}

//working
double fromJouleToElectronVolts( double joule ) {
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
}

//working
double fromMegatonToJoule( double megaton ) {
   //printf("megaton= %lf\n",megaton);
   //printf("divided by\n");
   //printf("megaton in joules = %e\n",MEGATON_TNT_IN_A_JOULE);

   return megaton/ MEGATON_TNT_IN_A_JOULE;
}
//working
double fromJouleToMegaton( double joule ) {
   //printf("megaton= %Lf\n",joule);
   //printf("times\n");
   //printf("megaton in joules = %e\n",MEGATON_TNT_IN_A_JOULE);
   return joule * MEGATON_TNT_IN_A_JOULE;
}
//working
double fromGasToJoule( double gas ) {
   //printf("gas= %lf\n",gas);
   //printf("divided by\n");
   //printf("gas in joules = %e\n",GAS_GAL_IN_A_JOULE);

   return gas / GAS_GAL_IN_A_JOULE;
}
//working
double fromJouleToGas( double joule ) {
   //printf("joule= %lf\n",joule);
   //printf("times\n");
   //printf("gas in joules = %e\n",GAS_GAL_IN_A_JOULE);
   return joule * GAS_GAL_IN_A_JOULE;
}
//working
double fromSupernovaToJoule( double supernova ) {
   //printf("supernova= %lf\n",supernova);
   //printf("divided by\n");
   //printf("supernova in joules = %e\n",SUPERNOVA_IN_A_JOULE);

   return supernova / SUPERNOVA_IN_A_JOULE;
}
//working
double fromJouleToSupernova( double joule ) {
   //printf("joule= %lf\n",joule);
   //printf("times\n");
   //printf("supernova in joules = %e\n",SUPERNOVA_IN_A_JOULE);
   return joule * SUPERNOVA_IN_A_JOULE;
}
//working
double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}
//working
double fromJouleToCatPower( double joule ) {
   return 0.0;  // Cats do no work
}



int main( int argc, char* argv[] ) {
   printf( "Energy converter\n" );

   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument

   //printf( "fromValue = [%lG]\n", fromValue );
   //printf( "fromUnit = [%c]\n", fromUnit );      
   //printf( "toUnit = [%c]\n", toUnit );          

   //switch statement to convert into a common value of joules
   double commonValue;
   switch( fromUnit ) {
      case JOULE         : commonValue = fromValue; // No conversion necessary
                          break;
      case ELECTRON_VOLT : commonValue = fromElectronVoltsToJoule( fromValue );
                          break;
      case MEGATON_TNT : commonValue = fromMegatonToJoule( fromValue ); 
                          break;
      case GAS_GAL_EQ : commonValue = fromGasToJoule(fromValue);
                          break;
      case SUPERNOVA : commonValue = fromSupernovaToJoule(fromValue);
                          break;
      case CATPOWER : commonValue = fromCatPowerToJoule(fromValue);
                          break;
      }

   //printf( "commonValue = [%lG] joule\n", commonValue ); 


	/// switch statement to convert common units to toUnit.  
   double toValue;
   switch (toUnit){
      case JOULE : toValue = commonValue;
                   break;
      case ELECTRON_VOLT : toValue = fromJouleToElectronVolts(commonValue);
                   break;
      case MEGATON_TNT : toValue = fromJouleToMegaton(commonValue);
                   break;
      case GAS_GAL_EQ : toValue = fromJouleToGas(commonValue);
                   break;
      case SUPERNOVA : toValue = fromJouleToSupernova(commonValue);
                   break;
      case CATPOWER : toValue = fromJouleToCatPower(commonValue);
                   break;
   }

   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
}
